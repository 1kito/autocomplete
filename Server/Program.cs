﻿using Core;
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Server
{
    class Program
    {
        static Autocomplete Autocomplete;

        static void Main(string[] args)
        {
            try
            {
                String path = "data/test.in";
                Int32 port = 12100;

                if (args != null && args.Length == 2)
                {
                    path = args[0];
                    port = Int32.Parse(args[1]);
                }

                while (true)
                {
                    TcpListener server = new ServerHandler(port).Listener;
                    Autocomplete = new Autocomplete();
                    try
                    {
                        // Определим нужное максимальное количество потоков
                        // Пусть будет по 4 на каждый процессор
                        Int32 maxThreadsCount = Environment.ProcessorCount * 4;
                        ThreadPool.SetMaxThreads(maxThreadsCount, maxThreadsCount);// Установим максимальное количество рабочих потоков
                        ThreadPool.SetMinThreads(2, 2);// Установим минимальное количество рабочих потоков

                        while (true)
                        {
                            Console.WriteLine("Ожидаем пользовательской активности... ");
                            ThreadPool.QueueUserWorkItem(RequestProcessing, server.AcceptTcpClient());
                        }
                    }
                    catch (SocketException e)
                    {
                        Console.WriteLine("SocketException: {0}", e);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exception: {0}", e);
                    }
                    finally
                    {
                        server.Stop();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Global exception: {0}", e);
            }
        }

        static void RequestProcessing(object client_obj)
        {
            try
            {
                using (TcpClient client = client_obj as TcpClient)
                {
                    using (NetworkStream networkStream = client.GetStream())
                    {
                        if (networkStream.CanRead)
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                byte[] myReadBuffer = new byte[1024];
                                StringBuilder entry = new StringBuilder();
                                Int32 numberOfBytesRead = 0;

                                do
                                {
                                    numberOfBytesRead = networkStream.Read(myReadBuffer, 0, myReadBuffer.Length);
                                    entry.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));
                                }
                                while (networkStream.DataAvailable);

                                // здесь обрабатываем ответ на запрос пользователя
                                entry = entry.Replace("get", "");
                                Console.WriteLine("Прислано сообщение: {0}", entry);

                                String answerMsg = Autocomplete.Search(entry.ToString());

                                Byte[] msg = Encoding.ASCII.GetBytes(answerMsg);
                                networkStream.Write(msg, 0, msg.Length);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Вы не можете прочитать данный из входного сетевого потока");
                        }
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
