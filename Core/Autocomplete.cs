﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Autocomplete
    {
        String Path { get; set; }
        Int32 N { get; set; }
        Int32 M { get; set; }
        List<WordFrequency> WordFrequencies { get; set; }

        public Autocomplete(Int32 n = 100000, Int32 m = 15000, String path = null)
        {
            N = n;
            M = m;
            Path = path ?? @"data\test.in";

            if (File.Exists(Path))
            {
                String[] all = File.ReadAllLines(Path);
                WordFrequencies = new List<WordFrequency>();
                for (Int32 i = 1; i < N + 1; i++)
                {
                    WordFrequency wf = new WordFrequency(all[i]);
                    WordFrequencies.Add(wf);
                }
            }
            else
            {
                Console.WriteLine("Не верно указан путь файла исходных данных");
            }
        }


        public string Search(String entry)
        {
            StringBuilder searchResult = new StringBuilder();
            if (!String.IsNullOrEmpty(entry))
            {
                IEnumerable<string> searching = (from w in WordFrequencies
                                                 where w.Word.IndexOf(entry) == 0
                                                 orderby w.Word
                                                 orderby w.Frequency descending
                                                 select w.Word);

                List<string> result = searching.Take(10).ToList();

                if (result.Any())
                {
                    result.ForEach(w => searchResult.AppendLine(w));
                }

            }
            return searchResult.ToString();
        }

        public String SearchAll()
        {
            List<UserEntry> userEntries = GetUserEntries();
            StringBuilder searchResult = new StringBuilder();
            Dictionary<String, String> cache = new Dictionary<String, String>();

            foreach (UserEntry item in userEntries)
            {
                String cacheResult = String.Empty;

                if (!cache.TryGetValue(item.Prefix, out cacheResult))
                {
                    cacheResult = Search(item.Prefix);
                    cache.Add(item.Prefix, cacheResult);
                }

                if (cacheResult != "")
                {
                    searchResult.AppendLine(cacheResult);
                }
            }

            return searchResult.ToString();
        }

        private List<UserEntry> GetUserEntries()
        {
            List<UserEntry> userEntries = new List<UserEntry>();

            String[] all = File.ReadAllLines(Path);
            for (Int32 i = N + 2; i < M + N + 2; i++)
            {
                UserEntry entry = new UserEntry(all[i]);
                userEntries.Add(entry);
            }

            return userEntries;
        }
    }
}
