﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Core
{
    public class ServerHandler
    {
        public TcpListener Listener;

        public ServerHandler(Int32 port = 12100, String ip = "127.0.0.1")
        {
            IPAddress localAddr = IPAddress.Parse(ip);
            Listener = new TcpListener(localAddr, port);
            Listener.Start();
        }

        ~ServerHandler()
        {
            if (Listener != null)
            {
                Listener.Stop();
            }
        }
    }
}
