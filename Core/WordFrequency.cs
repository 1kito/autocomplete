﻿using System;

namespace Core
{
    public class WordFrequency
    {
        public WordFrequency(String line)
        {
            String[] split = line.Split(new char[]{ ' ' });
            Word = split[0];
            Frequency = Int32.Parse(split[1]);
        }

        public String Word { get; set; }
        public Int32 Frequency { get; set; }
    }
}
