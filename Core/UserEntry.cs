﻿using System;

namespace Core
{
    class UserEntry
    {
        public String Prefix { get; set; }

        public UserEntry(String prefix)
        {
            Prefix = prefix;
        }
    }
}
