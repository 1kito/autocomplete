﻿using Core;
using System;
using System.Diagnostics;

namespace TestBase
{
    class Program
    {
        static void Main(string[] args)
        {
            Autocomplete testbase = new Autocomplete(5, 3, @"data\testbase.in");
            Console.WriteLine(testbase.SearchAll());

            Autocomplete testsort = new Autocomplete(5, 3, @"data\testsort.in");
            Console.WriteLine(testsort.SearchAll());

            Autocomplete test = new Autocomplete();
            Console.WriteLine("Загрузка ответов на 15000 запросов...");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Console.WriteLine(test.SearchAll());
            sw.Stop();
            Console.WriteLine("Затрачено: " + sw.ElapsedMilliseconds / 1000 + " sec");

            Console.ReadKey();
        }
    }
}
