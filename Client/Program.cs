﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    class Program
    {
        static String ServerIp = "127.0.0.1";
        static Int32 ServerPort = 12100;

        static void Main(string[] args)
        {
            if (args != null && args.Length == 2)
            {
                ServerIp = args[0];
                ServerPort = Int32.Parse(args[1]);
            }

            string prefix = "";
            ConsoleKeyInfo keyinfo;
            try
            {
                do
                {
                    while (!Console.KeyAvailable)
                    {
                        keyinfo = Console.ReadKey();
                        var key = keyinfo.KeyChar.ToString().ToLower();
                        if (keyinfo.Key == ConsoleKey.Backspace)
                        {
                            if (prefix != "")
                            {
                                prefix = prefix.Remove(prefix.Length - 1);
                                Console.SetCursorPosition(prefix.Length, 0);
                                Console.Write(new string(' ', Console.WindowWidth));
                            }
                        }
                        else
                        {
                            prefix += key;
                        }

                        OnNewInputLineEvent(prefix);
                    }
                } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }

        private static void OnNewInputLineEvent(string prefix)
        {
            String entry = "get" + prefix;

            using (TcpClient client = new TcpClient(ServerIp, ServerPort))
            {
                Byte[] data = Encoding.ASCII.GetBytes(entry);

                using (NetworkStream networkStream = client.GetStream())
                {
                    networkStream.Write(data, 0, data.Length);

                    Byte[] readBuffer = new Byte[256];
                    StringBuilder response = new StringBuilder();
                    Int32 numberOfBytesRead = 0;

                    do
                    {
                        numberOfBytesRead = networkStream.Read(readBuffer, 0, readBuffer.Length);
                        response.AppendFormat("{0}", Encoding.ASCII.GetString(readBuffer, 0, numberOfBytesRead));
                    }
                    while (networkStream.DataAvailable);

                    for (int i = 1; i < 12; i++)
                    {
                        Console.SetCursorPosition(0, i);
                        Console.Write(new string(' ', Console.WindowWidth));
                    }

                    Console.SetCursorPosition(0, 1);
                    Console.WriteLine("\n{0}", response);
                    Console.SetCursorPosition(prefix.Length, 0);
                }
            }
        }
    }
}
